---
layout: component
title: "Basic buttons"
type: buttons
scss_name: "_button.scss"
scss_path:
description: "Buttons are used to navigate through the website and for actions, like triggering a modal or validating a form. They should be (and are) easily distinguishable from another components."
sample:
    length: 2
    1:
        title: "Default"
        description:
        code: |
            <button class="c-btn">Click me por favor</button>
    2:
        title: "Primary"
        description:
        code: |
            <button class="c-btn c-btn--primary">Click me por favor</button>
---

