---
layout: component
title: "Lists"
type: typography
scss_name: "_list.scss"
scss_path:
description: "A list of items in which the order does not explicitly matter."
sample:
    length: 2
    1:
        title: Unordered list
        description: "A list of items in which the order does not explicitly matter."
        code: |
            <ul>
                <li>Lorem ipsum dolor sit amet</li>
                <li>Consectetur adipisicing elit</li>
                <li>Veniam deleniti quod</li>
                <li>Omnis quibusdam inventore</li>
                <li>Quia vel, repudiandae odit vero</li>
                <li>Accusantium nulla pariatur ea quos</li>
                <li>Modi cupiditate mollitia</li>
            </ul>
    2:
        title: Ordered list
        description: "A list of items in which the order matters."
        code: |
            <ol>
                <li>Lorem ipsum dolor sit amet</li>
                <li>Consectetur adipisicing elit</li>
                <li>Veniam deleniti quod</li>
                <li>Omnis quibusdam inventore</li>
                <li>Quia vel, repudiandae odit vero</li>
                <li>Accusantium nulla pariatur ea quos</li>
                <li>Modi cupiditate mollitia</li>
            </ol>
---



