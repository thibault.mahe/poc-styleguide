---
layout: component
title: "Generic card"
type: cards
scss_name: "_card.scss"
scss_path:
description: "Cards are the core components of the framework, allowing to build different views for different purposes and meanings. Each type of information, product or content could fit in its proper, resusable and flexible card component."
sample:
    length: 1
    1:
        title:
        description:
        code: |
            <div class="c-card">
                <img src="https://placehold.it/300x200" align="center" class="c-card__img">
                <div class="c-card__content">
                    <h2 class="c-card__title">Sample 1</h2>
                    <p class="c-p">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos ad pariatur nam totam in quas, sit modi maiores, tenetur voluptatem eveniet veniam officiis voluptatibus nobis quibusdam, impedit similique aspernatur commodi!</p>
                </div>
            </div>
---

